var controllers = controllers || new Controllers();

var BranchBrowserController = function(container, data){
  var self = this;
  self.template = Handlebars.compile(el(container, '.infobox').innerHTML);
  self.container = container;
  self.currentEnvironment = {};
  el(container, 'select').onchange = function(e){
    self.display(el(container, 'select').value);
  };
  self.display(el(container, 'select').value);
};

BranchBrowserController.prototype.display = function(environmentName, index){
  var self = this;
  var container = self.container;
  var view = el(container, '.infobox');
  index = index || 0;

  var getHostStatus = function(environment, callback){
    environment.hostsInfo = [];
    async.each(environment.hosts||[], function(host, next){
      Loader.get('/api/v1/status/'+host, function(err, status){
        var info = err?{
          error: err.error||err
        }: (status.error || status.code || status.response || (typeof(status)==='string')?{
          error: status.error||status.code||status.response||status
        }:{
          status: status
        });
        info.ip = host;
        info.port = environment.hostPort||8080;
        environment.hostsInfo.push(info);
        next();
      });
    }, function(){
      callback(null, environment);
    });
  };
  
  var displayEnvironmentDeploy = function(deploy){
    getHostStatus(deploy, function(err, environment){
      var output;
      if(err){
        output = '<pre><b>ERROR:</b>'+JSON.stringify(err, null, '  ')+'</pre>';
        return;
      }else{
        environment.imageVersions = [];
        self.currentEnvironment.deploys.forEach(function(deploy){
          environment.imageVersions.push({version: deploy.imageVersion, date: deploy._created});
        });
        output = self.template(environment, {helpers: handlebarsHelpers});
      }
      el(container, '.infobox').innerHTML = output;
      el(container, 'select').value = environment.environment;
    });
  };
  
  partials.get('loading', function(err, ltemplate){
    view.innerHTML = ltemplate();
    if(self.currentEnvironment !== environmentName){
      self.getEnvironmentDeploys(environmentName, function(){
        displayEnvironmentDeploy(self.currentEnvironment.deploys[index]);
      });
    }else{
      displayEnvironmentDeploy(self.currentEnvironment.deploys[index]);
    }
  });
};

BranchBrowserController.prototype.getEnvironmentDeploys = function(environmentName, callback){
  var self = this;
  Loader.get('/api/v1/environment/'+environmentName, function(err, deploys){
    self.currentEnvironment = {
      name: environmentName,
      deploys: (deploys||{}).items
    };
    callback(err, deploys);
  });
};

controllers.register('BranchBrowser', BranchBrowserController);
