var controllers = controllers || new Controllers();

var MonitorController = function(container, data){
  var noop = function(){};
  var template = Handlebars.compile(container.innerHTML);
  var updateDetails = function(pkt){
    try{
      var url = pkt.url.split('/').map(function(src, i){
        if(src.match(/^([a-z_0-9]+[a-z0-9_-]+)+$/i)&&src.match(/[0-9]/)){
          return '{arg'+i+'}';
        }else if(src.match(/sectionKeys:[a-z0-9]+$/i)){
          return 'sectionKeys:{arg'+i+'}';
        }else{
          return src;
        }
      }).join('/');
      var id = url.replace(/[^a-z0-9]/g, '');
      var elem = el('#'+id);
      pkt.duration = Math.ceil(pkt.duration);
      if(!elem){
        elem = document.createElement('div');
        elem.id = id;
        elem.info = elem.info||{
          url: url,
          ttl: 0,
          count: 0,
          min: 99999999,
          max: 0
        };
        container.appendChild(elem);
      }
      elem.info.ttl += pkt.duration;
      if(pkt.duration<elem.info.min){
        elem.info.min = pkt.duration;
      }
      if(pkt.duration>elem.info.max){
        elem.info.max = pkt.duration;
      }
      elem.info.max = pkt.duration;
      elem.info.lastDuration = pkt.duration;
      elem.info.lastCompleted = new Date(pkt.completed);
      if(elem.info.recordSlow){
        elem.info.windowSlow++;
      }
      if(pkt.duration>1000){
        if(!elem.info.startedSlowdown){
          elem.info.startedSlowdown = new Date(pkt.completed);
          elem.info.numSlow = 1;
          elem.info.windowSlow = 1;
        }else{
          elem.info.numSlow++;
        }
        elem.info.lastOver1Second = new Date(pkt.completed);
        elem.info.style = 'warning';
        elem.info.recordSlow = true;
        var p = elem.parentNode;
        if(p.childNodes.length>1){
          p.removeChild(elem);
          p.insertBefore(elem, p.childNodes[0]);
        }
      }else if(elem.info.lastOver1Second &&
        (new Date()-elem.info.lastOver1Second.getTime())<5000){
        elem.info.style = 'warning';
      }else{
        elem.info.recordSlow = false;
        elem.info.style = '';
      }
      elem.innerHTML = template(elem.info, {helpers: handlebarsHelpers});
    }catch(e){
      console.error(e);
    }
  };
  socket.on('disconnect', function(){
    el('.pageTitle').style.color='red';
  });
  socket.on('monitor::line', noop);
  socket.on('monitor::update', updateDetails);
  socket.emit('monitor::connect', data.hosts);
  container.innerHTML = '';
};

controllers.register('MonitorController', MonitorController);
