var controllers = controllers || new Controllers();

var ScriptEditorController = function(container, data){
  var btnSave = el(container, '.save');
  var btnNew = el(container, '.new');
  var btnLoad = el(container, '.load');
  var btnExec = el(container, '.execute');
  if(data.host){
    el(container, '.host').value = data.host;
  }
  var editor = CodeMirror.fromTextArea(el(container, '#source'), {
    mode: "application/javascript",
    lineNumbers: true,
    styleActiveLine: true,
    matchBrackets: true
  });
  btnExec.onclick = function(e){
    e.preventDefault();
    var user = el(container, '.user').value;
    var host = el(container, '.host').value;
    var port = el(container, '.port').value;
    var source = editor.getValue();
    if(!user){
      humane.remove();
      return humane.error('Must supply a user');
    }
    if(!host){
      humane.remove();
      return humane.error('Must supply a host');
    }
    if(!source){
      humane.remove();
      return humane.error('Must supply something to run');
    }
    btnExec.style.display = 'none';
    el(container, '.output').style.display = 'none';
    Loader.post('/api/v1/ssh?user='+user+'&host='+host+'&port='+port, {data: source}, function(err, resp){
      btnExec.style.display = '';
      if(err){
        humane.remove();
        humane.error(err.error||err);
        el(container, '.output pre').innerHTML = ANSI.toHtml(err.error||(typeof(err)==='object'?JSON.stringify(err):err));
        el(container, '.output').style.display = '';
        return;
      }
      el(container, '.output').style.display = '';
      if(resp instanceof Array){
        var html = '';
        resp.forEach(function processResponseItem(resp){
          html += '<h1>'+resp.host+'</h1>\r\n';
          if(resp.error){
            html += JSON.stringify(resp.error, null, '  ') + '\r\n';
          }else if(typeof(resp.data)==='string'){
            html += ANSI.toHtml(resp.data) + '\r\n';
          }else{
            html += JSON.stringify(resp.data, null, '  ') + '\r\n';
          }
        });
        el(container, '.output pre').innerHTML = html;
      }else{
        if(typeof(resp)==='string'){
          el(container, '.output pre').innerHTML = ANSI.toHtml(resp);
        }else{
          el(container, '.output pre').innerText = JSON.stringify(resp, null, '  ');
        }
      }
    });
    return false;
  };
  btnNew.onclick = function(e){
    e.preventDefault();
    el('#name').value = '';
    editor.setValue('');
    el(container, 'select').value = '';
    return false;
  };
  el(container, 'select').onchange = btnLoad.onclick = function(e){
    e.preventDefault();
    var id = el(container, 'select').value;
    btnExec.style.display = 'none';
    if(!id){
      humane.remove();
      return humane.error('You must select something to load');
    }
    Loader.get('/api/v1/script/'+id, function(err, resp){
      if(err){
        humane.remove();
        return humane.error(err);
      }
      el(container, '#name').value = resp.name;
      editor.setValue(resp.source);
      btnExec.style.display = '';
    });
    return false;
  };
  btnSave.onclick = function(e){
    e.preventDefault();
    var pkt = {
        name: el('#name').value,
        source: editor.getValue()
      },
      sel = el(container, 'select'),
      id = sel.value;
      url = '/api/v1/script';
    if(id){
      url += '/'+id;
    }
    if(!pkt.name){
      humane.remove();
      return humane.error('Must supply a name');
    }
    if(!pkt.source){
      humane.remove();
      return humane.error('Must supply some code');
    }
    Loader.post(url, {data: pkt}, function(err, resp){
      if(err){
        humane.remove();
        humane.error(err);
        return;
      }
      if(!id){
        var opt = document.createElement('option');
        opt.value = resp._id;
        opt.text = resp.name;
        sel.appendChild(opt);
        sel.value = resp._id;
      }
      humane.remove();
      humane.log('saved');
    });
    return false;
  };
  el(container, 'select').value = '';
  el(container, '.output').style.display = 'none';
};

controllers.register('ScriptEditor', ScriptEditorController);
