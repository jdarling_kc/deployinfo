(function(global){
  global.socket = io();

  var partials = global.partials = new Partials({
    path: "partials/",
    ext: ".html"
  });
  var controllers = global.controllers = (global.controllers || new Controllers());

  humane.error = humane.spawn({addnCls: 'humane-original-error' });

  var Application = global.Application = function(){
    var self = this;
    self.init();
  };

  Application.prototype.displayPage = function(pageName, data){
    var path = pageName.split('/');
    var nav = path.shift();

    partials.get(pageName, function(err, template){
      if(err){
        throw new Error(err);
      }
      try{
        var pane = el('#outlet');
        var controllerName = el('#'+pageName).getAttribute('data-controller');
        if(nav==='index'){
          nav = el('nav li a[href="#home"]');
        }else{
          nav = el('nav li a[href="#'+(nav||'home')+'"]');
        }
        pane.innerHTML = template(data||{}, {helpers: handlebarsHelpers});
        if(controllerName){
          controllers.create(pane, controllerName, data);
        }
        var elm, elms = els(pane, '[data-controller]'), i, l=elms.length;
        for(i=0; i<l; i++){
          elm = elms[i];
          controllerName = elm.getAttribute('data-controller');
          controllers.create(elm, controllerName, data);
        }
      }catch(e){
        throw e;
      }
    });
  };

  Application.prototype.init = function(){
    var app = this;
    var nav = Satnav({
      html5: false,
      force: true,
      poll: 100
    });

    nav
      .navigate({
        path: '/',
        directions: function(params){
          Loader.get('/api/v1/environments', function(err, list){
            var items = list.items;
            app.displayPage('home', items);
          });
        }
      })
      .navigate({
        path: '/scripts',
        directions: function(params){
          Loader.get('/api/v1/scripts', function(err, list){
            list.items.sort(function(a, b){
              return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
            });
            app.displayPage('scripts', list);
          });
        }
      })
      .navigate({
        path: '/scripts/{host}',
        directions: function(params){
          Loader.get('/api/v1/scripts', function(err, list){
            list.items.sort(function(a, b){
              return (a.name||'').toLowerCase().localeCompare((b.name||'').toLowerCase());
            });
            list.host = params.host;
            app.displayPage('scripts', list);
          });
        }
      })
      .navigate({
        path: '/monitor/{environment}/{hosts}',
        directions: function(params){
          app.displayPage('monitor', params);
        }
      })
      .change(function(params, old){
        app.displayPage('loading');
        nav.resolve();
        return this.defer;
      })
      .otherwise('/');
      ;
    nav.go();
  };

  var app = global.app = new Application();
})(this);
