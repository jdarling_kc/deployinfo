var reBlockBegin = /^HTTP Call Complete:\s(.+)$/;
var reIsDateLine = /(started|completed):\s(.+)?,$/;
var reShorten = /^[a-z0-9]*[0-9]+[^?]*$/g;
var readline = require('readline');
var stream = require('stream');

module.exports = function(instream, options){
  var self = this;
  var doneCallback, matchCallback, lineCallback;
  self.options = options || {};
  self.doneCallback = self.options.onDone || function(){};
  self.matchCallback = self.options.onMatch || function(){};
  self.lineCallback = self.options.onLine || function(){};

  self.outstream = new stream;
  self.outstream.readable = true;
  self.outstream.writable = true;
  self.inBlock = false;
  self.buffer = '';
  self.lines = 0;

  self.rl = readline.createInterface({
      input: instream,
      output: self.outstream,
      terminal: false
    });

  self.rl.on('line', function(raw) {
    var match, block;
    var line = raw.toString();
    self.lines++;
    self.lineCallback(self.lines, line);
    if(self.inBlock){
      if(match = reIsDateLine.exec(line)){
        line = match[1]+': new Date("'+match[2]+'"),';
      }
      try{
        var f = new Function('return '+self.buffer+line);
        block = f();
      }catch(e){
        block = false;
      }
      if(block){
        self.inBlock = false;
        var link = block.uri.href.split('?').shift().split('/').map(function(seg, i){
          return seg.match(reShorten)?'{arg'+i+'}':seg;
        }).join('/');
        self.matchCallback(link, block.duration, block);
        self.buffer = '';
      }else{
        self.buffer += line+'\n';
      }
    }else{
      if(match = reBlockBegin.exec(line.toString())){
        self.inBlock = true;
        self.buffer = match[1]+'\n';
      }
    }
  });

  self.rl.on('close', function(){
    self.doneCallback();
  });
}
