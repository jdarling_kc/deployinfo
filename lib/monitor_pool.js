var Connection = require('ssh2');
var Parser = require('./parse_stream');
var noop = function(){};
var config = require('../lib/config').config.sshremote||{};
var certFile = require('fs').readFileSync(config.certfile||'certificates/id_rsa.pem');
var async = require('async');

var HostConnection = function(monitor, ip, options){
  var self = this;
  var opts = {
    host: ip,
    port: 22,
    username: 'ubuntu',
    privateKey: certFile
  };
  var logPath = '/var/log/console-ui/console-ui.log';
  var cmd = 'sudo tail -f --lines=200 '+logPath;

  var conn = self.conn = new Connection();
  self.options = options;
  self.ip = ip;

  conn.on('ready', function(){
    (options.onConnect||noop)(ip, conn);
    // TODO: really should be looking up the location to the logs,
    // for now will just hard code the thing to get it working
    conn.exec(cmd, function(err, stream){
      if(err){
        console.error(err.stack||err);
        process.exit(1);
      }
      new Parser(stream, {
        onLine: function(lineNumber, source){
          (options.onLine||noop)(ip, lineNumber, source);
        },
        onMatch: function(url, duration, block){
          (options.onMatch||noop)(ip, url, duration, block);
        },
        onDone: function(){
          monitor.hosts[ip] = false;
          conn.end();
          (options.onDisconnect||noop)(ip, conn);
        }
      });
    });
  }).connect(opts);
  monitor.hosts[ip] = self;
};

/*
options: {
  onConnect: function(ip, connection),
  onLine: function(ip, lineNumber, source),
  onMatch: function(ip, url, duration, block),
  onDisconnect: function(ip, connection)
}
*/
var MonitorPool = function(hosts, opts){
  var self = this;
  var options = opts || {};
  var onDisconnect;
  hosts = (typeof(hosts)==='string')?hosts.split(',').map(function(ip){
    return ip.trim();
  }):hosts;
  self.hosts = {};
  hosts.forEach(function(ip){
    new HostConnection(self, ip, options);
  });
};

MonitorPool.prototype.connect = function(ip, callback){
  var self = this;
  if(self.hosts[ip]){
    return;
  }
  new HostConnection(self, ip, self.options, callback||noop);
};

MonitorPool.prototype.disconnect = function(callback){
  var self = this;
  async.each(self.hosts, function(connection, next){
    connection.conn.options.onDisconnect = function(){
      next();
    };
    connection.conn.end();
  }, function(){
    self.hosts = {};
    (callback||noop)();
  });
};

module.exports = MonitorPool;
