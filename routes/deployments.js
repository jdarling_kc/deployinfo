var logger = require('../lib/logger');
var deployes = require('../lib/store')('deploys');
var hosts = require('../lib/store')('hosts');
var features = require('../lib/store')('features');
var environments = require('../lib/store')('environments');
var async = require('async');
var extend = require('util')._extend;

var listDeployments = function(request, reply){
  deployes.asArray(request.query, function(err, response){
    reply(err||response);
  });
};

var getDeployment = function(request, reply){
  hosts.get(request.params.id, function(err, response){
    reply(err||response);
  });
};

var postDeployment = function(request, reply){
  var deployments = request.payload instanceof Array?request.payload:[request.payload];
  var results = [];
  async.each(deployments, function(deployment, nextDeployment){
    deployment.feature = deployment.feature || deployment.branch || 'master';
    
    async.each(deployment.hosts, function(host, next){
      var rec = extend({ip: host, host: host}, deployment);
      hosts.upsert({ip: host}, rec, function(){
        next();
      });
    }, function(){
      features.upsert({feature: deployment.feature}, deployment, function(){
        environments.upsert({feature: deployment.feature, environment: deployment.environment}, deployment, function(){
          deployes.insert(deployment, function(err, record){
            results.push(record);
            nextDeployment();
          });
        });
      });
    });
  }, function(){
    reply(results.length>1?results:results[0]);
  });
};

module.exports = function(server, config){
  server.route([
    {
      method: 'GET',
      path: config.route+'deployments',
      handler: listDeployments
    },
    {
      method: 'GET',
      path: config.route+'deployment/{id}',
      handler: getDeployment
    },
    {
      method: 'POST',
      path: config.route+'deployment',
      handler: postDeployment
    },
  ]);
};