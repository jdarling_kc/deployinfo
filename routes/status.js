var logger = require('../lib/logger');
var hosts = require('../lib/store')('hosts');
var async = require('async');
var request = require('request');

var checkHost = function(ip, port, callback){
  request({
    method: 'GET',
    uri: 'http://'+ip+':'+port+'/status',
    timeout: 500,
    json: true
  }, function(error, response, body){
    if(error){
      request({
        method: 'GET',
        uri: 'http://'+ip+':'+port+'/status',
        timeout: 500,
        json: true
      }, function(error, response, body){
        callback(error, body);
      });
    }else{
      callback(error, body);
    }
  });
};

var getStatus = function(request, reply){
  var ip = request.params.ip, results = [];
  hosts.asArray({filter: {ip: ip}}, function(err, response){
    if(err){
      return reply(err);
    }
    var results = [];
    var done = function(){
      if(results.length===0){
        console.log(id, 'FAIL');
      }
      reply(results[0]);
    };
    var recs = response[response.root];
    async.each(recs, function(rec, next){
      checkHost(rec.host||rec.ip, rec.hostPort||8080, function(err, resp){
        var result = err||resp;
        if(typeof(result)==='string'){
          result = {_id: rec.host||rec.ip, response: result};
        }
        if(!result){
          result = {_id: rec.host||rec.ip};
        }
        results.push(result);
        next();
      });
    }, done);
  });
};

module.exports = function(server, config){
  server.route([
    {
      method: 'GET',
      path: config.route+'status/{ip}',
      handler: getStatus
    },
    {
      method: 'GET',
      path: config.route+'status/{ip}/{port}',
      handler: getStatus
    },
  ]);
};
