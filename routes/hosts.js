var logger = require('../lib/logger');
var hosts = require('../lib/store')('hosts');

var listHosts = function(request, reply){
  hosts.asArray(request.query, function(err, response){
    reply(err||response);
  });
};

var getHost = function(request, reply){
  hosts.asArray({filter: {ip: request.params.ip}}, function(err, response){
    var resp = (response.response||[]).shift();
    reply(err||resp);
  });
};

module.exports = function(server, config){
  server.route([
    {
      method: 'GET',
      path: config.route+'hosts',
      handler: listHosts
    },
    {
      method: 'GET',
      path: config.route+'host/{ip}',
      handler: getHost
    },
  ]);
};