var logger = require('../lib/logger');
var environments = require('../lib/store')('environments');
var deployes = require('../lib/store')('deploys');
var semver = require('semver');

var listEnvironments = function(request, reply){
  environments.asArray(request.query, function(err, response){
    reply(err||response);
  });
};

var getEnvironment = function(request, reply){
  var params = request.query;
  params.filter = params.filter || {};
  params.filter.environment=request.params.name;
  params.sort = params.sort || {_created: -1};

  deployes.asArray(params, function(err, response){
    if(!err){
      response[response.root] = response[response.root].sort(function(e1, e2){
        return semver.compare(e2.imageVersion, e1.imageVersion);
      });
    }
    reply(err||response);
  });
};

var getEnvironmentFeature = function(request, reply){
  var params = request.query;
  params.filter = params.filter || {};
  params.filter.environment=request.params.name;
  params.filter.feature=request.params.feature;
  params.sort = params.sort || {_created: -1};

  deployes.asArray(params, function(err, response){
    //var resp = (response.response||[]).shift();
    reply(err||response);
  });
};

module.exports = function(server, config){
  server.route([
    {
      method: 'GET',
      path: config.route+'environments',
      handler: listEnvironments
    },
    {
      method: 'GET',
      path: config.route+'environment/{name}',
      handler: getEnvironment
    },
    {
      method: 'GET',
      path: config.route+'environment/{name}/feature/{feature}',
      handler: getEnvironmentFeature
    },
  ]);
};
