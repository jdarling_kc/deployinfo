var logger = require('../lib/logger');
var features = require('../lib/store')('features');

var listFeatures = function(request, reply){
  features.asArray(request.query, function(err, response){
    reply(err||response);
  });
};

var getFeature = function(request, reply){
  features.asArray({filter: {feature: request.params.name}}, function(err, response){
    var resp = (response.response||[]).shift();
    reply(err||resp);
  });
};

module.exports = function(server, config){
  server.route([
    {
      method: 'GET',
      path: config.route+'features',
      handler: listFeatures
    },
    {
      method: 'GET',
      path: config.route+'feature/{name}',
      handler: getFeature
    },
  ]);
};