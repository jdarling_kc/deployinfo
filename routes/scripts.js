var scripts = require('../lib/store')('scripts');

var listScripts = function(request, reply){
  scripts.asArray(request.query, function(err, docs){
    if(err){
      return reply(err);
    }
    var recs = docs[docs.root], i, l = recs.length;
    for(i=0; i<l; i++){
      recs[i] = {
        _id: recs[i]._id,
        name: recs[i].name
      };
    }
    reply(docs);
  });
};

var getScript = function(request, reply){
  scripts.get(request.params.id, function(err, doc){
    if(err){
      return reply(err);
    }
    reply(doc);
  });
};

var createScript = function(request, reply){
  scripts.insert(request.payload,  function(err, doc){
    if(err){
      return reply(err);
    }
    reply(doc);
  });
};

var updateScript = function(request, reply){
  scripts.update(request.params.id, request.payload,  function(err, doc){
    if(err){
      return reply(err);
    }
    reply(doc);
  });
};

var deleteScript = function(request, reply){
  scripts.delete(request.params.id,  function(err){
    if(err){
      return reply(err);
    }
    reply('Deleted');
  });
};

module.exports = function(server, config){
  server.route([
    {
      method: 'GET',
      path: config.route+'scripts',
      handler: listScripts
    },
    {
      method: 'GET',
      path: config.route+'script/{id}',
      handler: getScript
    },
    {
      method: 'POST',
      path: config.route+'script',
      handler: createScript
    },
    {
      method: 'POST',
      path: config.route+'script/{id}',
      handler: updateScript
    },
    {
      method: 'DELETE',
      path: config.route+'script/{id}',
      handler: deleteScript
    },
  ]);
};