var fs = require('fs');
var Connection = require('ssh2');
var vm = require('vm');
var certFile;
var async = require('async');
var MonitorPool = require('../lib/monitor_pool');
var logger = require('../lib/logger');
var domain = require('domain');

var execute = function(options, callback){
  var c = new Connection();
  var response = [];

  c.runCommand = function(command, callback){
    this.exec(command, function(err, stream){
      var out = {};
      if(err){
        console.error('ERROR:c.runCommand->this.exec', err);
        return callback(err);
      }
      stream.on('data', function(data, extended) {
        extended = extended || 'stdout';
        out[extended] = (out[extended]||'') + data.toString();
      });
      stream.on('error', function(e){
        console.log('ERROR:c.runCommand', arguments);
        response.push(e);
      });
      stream.on('end', function(code, signal) {
        out.code = code;
        return callback(null, out);
      });
    });
  };

  c.on('ready', function() {
    var
      m = {
        exports: {}
      },
      d = domain.create(),
      wrap = function(f){
        return function(){
          try{
            return f.apply(this, arguments);
          }catch(e){
            console.log('ERROR:wrap', e);
            response.push({error: e.stack||e.toString()});
            c.end();
            return callback(null, response.length>1?response:response.shift()||'');
          }
        }
      },
      sandbox = {
        module: m,
        exports: m.exports,
        connection: c,
        console: console,
        require: require,
        run: function(command, callback){
          try{
            c.runCommand(command, wrap(callback));
          }catch(e){
            console.log('ERROR:c.runCommand', e);
            response.push({error: e});
          }
        },
        part: function(part){
          response.push(part);
        },
        done: function(resp){
          if(resp){
            response.push(resp);
          }
          callback(null, response.length>1?response:response.shift()||'');
          c.end();
          d.dispose();
        }
      };
    try{
      var script = '"use strict";\n'+options.script;
      //vm.runInNewContext(script, sandbox);
      var f = new Function('module, exports, connection, console, require, run, part, done', script);
      d.on('error', function(err){
        console.log('ERROR:domain', err);
        response.push({error: err.stack||err});
        callback(null, response.length>1?response:response.shift()||'');
      });
      f(m, m.exports, c, console, require, sandbox.run, sandbox.part, sandbox.done);
    }catch(e){
      console.log('ERROR:vm.runInNewContext', e);
      response.push({error: e.stack||e});
      callback(null, response.length>1?response:response.shift()||'');
    }
  });
  c.on('error', function(err){
    console.error('ERROR:c.on', err);
    c.end();
    return callback(err);
  });
  c.on('end', function(){
  });
  c.on('close', function(had_error){
  });

  c.connect(options);

  return c;
};

var runRemote = function(request, reply){
  var user = request.query.user;
  var hosts = (request.query.host||'').split(',').map(function trim(s){return s.trim();});
  var port = parseInt(request.query.port || 22, 10);
  var script = request.payload;
  var replies = [];
  var handleResponse = function(err, data){
    if(err){
      return reply(err.toString());
    }
    return reply(data)
  };
  if(!user){
    return reply('No user provided');
  }
  if(!hosts.length){
    return reply('No host(s) provided');
  }

  async.each(hosts, function(host, next){
    execute({
      host: host,
      port: port,
      username: user,
      privateKey: certFile,
      script: script
    }, function(err, data){
      if(err){
        replies.push({
          host: host,
          error: err
        });
        return next();
      }
      replies.push({
        host: host,
        data: data
      });
      return next();
    });
  }, function(){
    if(replies.length>1){
      return reply(replies);
    }
    var resp = replies.shift()||{};
    return reply(resp.error||resp.data);
  });
};

var monitorLogs = function(data, callback, skipDisconnect){
  var socket = this;
  var hosts = typeof(data)==='string'?data.split(',').map(function(ip){
    return ip.trim();
  }):(data||{}).hosts;
  if(!(hosts instanceof Array)){
    callback('No hosts provided to connect to or invalid hosts provided.');
  }
  socket.inConnect=true;
  if(socket.monitor && !skipDisconnect){
    logger.log('Disconnecting: ', hosts);
    return socket.monitor.disconnect(function(){
      monitorLogs.call(socket, data, callback, true);
    });
  }
  logger.log('Connecting to: ', hosts);
  socket.monitor = new MonitorPool(hosts, {
    onConnect: function(ip, connection){
      socket.emit('monitor::connected', {ip: ip});
      socket.inConnect=false;
    },
    onLine: function(ip, lineNumber, source){
      socket.emit('monitor::line', {ip: ip, lineNumber: lineNumber, line: source});
    },
    onMatch: function(ip, url, duration, block){
      socket.emit('monitor::update', {ip: ip, url: url, duration: duration, raw: block.uri.href, completed: block.completed});
    },
    onDisconnect: function(ip, connection){
      socket.emit('monitor::disconnected', {ip: ip});
      logger.log('Disconnected: '+ip)
      if(!socket.inConnect){
        logger.log('Reconnecting '+ip)
        socket.monitor.connect(ip);
      }
    }
  });
  socket.on('disconnect', function(disconnectReason){
    logger.log(disconnectReason+':', hosts);
    if(socket.monitor){
      socket.monitor.disconnect();
      socket.monitor = false;
      logger.log('socket closed');
    }
  });
  callback('Connecting');
};

module.exports = function(server, config, sockets){
  certFile = fs.readFileSync(config.certfile||'certificates/id_rsa.pem');
  server.route([
    {
      method: 'POST',
      path: config.route+'ssh',
      handler: runRemote
    },
  ]);

  sockets.on('monitor::connect', monitorLogs);
};
